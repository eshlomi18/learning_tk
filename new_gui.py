import math
import tkinter as tk
import pyproj
from PIL import Image, ImageTk
import os
import my_generator.my_detection_generator
from conversions import new_polar2gps, new_gps2polar
import my_generator.utils
import sys
from rtiwrapper.RtiConnectorWrapper import RtiConnectorWrapper
import json
import random
import uuid
import struct

transformer = pyproj.Transformer.from_proj(4326, pyproj.Proj(proj='utm', zone=36, ellps='WGS84'))


class DraggableWidget:
    def __init__(self, widget):
        self.widget = widget
        self.widget.bind("<Button-1>", self.on_button_press)
        self.widget.bind("<ButtonRelease-1>", self.on_button_release)
        self.widget.bind("<B1-Motion>", self.on_button_motion)
        self.widget.bind("<Double-Button-1>", self.on_double_click)
        self.dragging = False

    def on_button_press(self, event):
        if self.dragging:
            self.start_x = event.x_root
            self.start_y = event.y_root
            self.widget_x = self.widget.winfo_x()
            self.widget_y = self.widget.winfo_y()

    def on_button_release(self, event):
        pass

    def on_button_motion(self, event):
        if self.dragging:
            x = event.x_root - self.start_x + self.widget_x
            y = event.y_root - self.start_y + self.widget_y
            self.widget.place(x=x, y=y)

    def on_double_click(self, event):
        # the x and y points of the clicked widget exept from the "publish" button
        if self.widget.cget("text") != "publish":
            print(f"x={self.widget.winfo_x()}, y={self.widget.winfo_y()}")


class App:
    # NOTE TO MYSELF: every time you add widget you need to give his name and position in the widget_positions.txt file
    # Pay attention not to leave empty lines in the file, not even at the end of the file
    # every time I create a new attribute I need to pay attention that his name
    # is in the list of the not_in_the_name list in the save method
    def __init__(self):
        self.root = tk.Tk()
        self.root.title("GUI")
        self.root.geometry("670x500")

        # placing the root window
        self.root.geometry('+{}+{}'.format((self.root.winfo_screenwidth() // 2) - (self.root.winfo_reqwidth() // 2),
                                           (self.root.winfo_screenheight() // 4) - (self.root.winfo_reqheight() // 4)))
        labels_widgets = {
            "source_id": {"text": "source_id"},
            "classification": {"text": "classification"},
            "classscore": {"text": "classscore"},
            "tank_location_available": {"text": "tank_location_available", "font": "bold"},
            "tank_altitude": {"text": "tank_altitude"},
            "tank_altitude_measurement_units": {"text": "M"},
            "tank_latitude": {"text": "tank_latitude"},
            "tank_longitude": {"text": "tank_longitude"},
            "target_polar_available": {"text": "target_polar_available", "font": "bold"},
            "elevation": {"text": "elevation"},
            "azimuth": {"text": "azimuth"},
            "range": {"text": "range"},
            "range_measurement_units": {"text": "M"},
            "target_geo_available": {"text": "target_geo_available", "font": "bold"},
            "target_altitude": {"text": "target_altitude"},
            "target_altitude_measurement_units": {"text": "M"},
            "target_latitude": {"text": "target_latitude"},
            "target_longitude": {"text": "target_longitude"},
            "uniq_id": {"text": "uniq_id"},
            "msb": {"text": "msb"},
            "lsb": {"text": "lsb"},
            "type": {"text": "type"},
            # add more labels here if needed
        }
        for name, props in labels_widgets.items():
            label = tk.Label(self.root, text=props["text"], font=props.get("font"))  # label text and font
            draggable_widget = DraggableWidget(label)
            setattr(self, f"{name}", label)  # create an attribute on the instance that references the Label object
            setattr(self, f"draggable_{name}", draggable_widget)

        # create a dictionary of options for each menu
        menu_widgets = {
            "source_id": {"default": "1.2.1", "values": ["1.14.1", "1.21.1", "1.2.1", "1.22.1", "1.10.1", "1.11.1"],
                          "command": self.on_version_selected},
            "classification": {"default": "General Enemy",
                               "values": ["Person", "Car", "General Enemy", "Infantry", "AT", "AFV", "Motorcycle",
                                          "Windcoat", "Marker"]},
            "tank_location_available": {"default": "True", "values": ["True", "False"], "command": self.menu_clicked},
            "tank_latitude": {"default": "deg", "values": ["deg", "rad"], "command": self.deg_rad},
            "tank_longitude": {"default": "deg", "values": ["deg", "rad"], "command": self.deg_rad},
            "target_polar_available": {"default": "True", "values": ["True", "False"], "command": self.menu_clicked},
            "elevation": {"default": "deg", "values": ["deg", "rad"], "command": self.deg_rad},
            "azimuth": {"default": "deg", "values": ["deg", "rad"], "command": self.deg_rad},
            "target_geo_available": {"default": "True", "values": ["True", "False"], "command": self.menu_clicked},
            "target_latitude": {"default": "deg", "values": ["deg", "rad"], "command": self.deg_rad},
            "target_longitude": {"default": "deg", "values": ["deg", "rad"], "command": self.deg_rad},
            "type": {"default": "DETECTION", "values": ["DETECTION", "THREAT", "TARGET"],
                     "command": self.type_selected},
            # add more menus here if needed
        }
        # create an option menu for each option in the dictionary
        for name, props in menu_widgets.items():
            default = props["default"]
            values = props["values"]
            command = props.get("command")  # optional, use None if not specified

            # create a StringVar to keep track of the selected value
            var = tk.StringVar(self.root)
            var.set(default)

            # create the OptionMenu widget
            menu = tk.OptionMenu(self.root, var, *values, command=command)

            # add the draggable widget functionality
            draggable_menu = DraggableWidget(menu)

            # save the menu and its associated StringVar as instance attributes
            setattr(self, f"{name}_menu", menu)
            setattr(self, f"{name}_value_inside", var)
            setattr(self, f"draggable_{name}_menu", draggable_menu)

            # if classification menu, update the default value based on the selected source_id
            if name == "classification":
                var.trace_add("write", lambda *_: self.on_version_selected)
            if name == "type":  # default value for the first run
                self.value = 0

        entry_widgets = {
            "classscore": {"default_value": "70.0"},
            "tank_altitude": {"default_value": "37.0"},
            "tank_latitude": {"default_value": "31.993745509007443"},
            "tank_longitude": {"default_value": "34.734013371829604"},
            "elevation": {"default_value": "-1.3507144302751073"},
            "azimuth": {"default_value": "-147.41850983474833"},
            "range": {"default_value": "646.0"},
            "target_altitude": {"default_value": "21.7723390767849"},
            "target_latitude": {"default_value": "31.988887108275428"},
            "target_longitude": {"default_value": "34.73024022450558"},
            "msb": {"default_value": "-7232807926196496584"},
            "lsb": {"default_value": "2647935891334119835"},
            # add more entry here if needed
        }
        for name, props in entry_widgets.items():
            entry = tk.Entry(self.root)
            entry.insert(0, str(props["default_value"]))
            draggable_widget = DraggableWidget(entry)
            setattr(self, f"{name}_entry", entry)
            setattr(self, f"draggable_{name}_entry", draggable_widget)

        # creating canvas with a selected image and adding it to the gui window
        image_widgets = {
            "tank": {"file": "merkava4.jpg", "width": 70, "height": 50},
            "mantak": {"file": "mantak.png", "width": 70, "height": 50},
            "ministry_of_defence": {"file": "ministry_of_defence.png", "width": 70, "height": 60},
        }
        for name, props in image_widgets.items():
            canvas = tk.Canvas(self.root, width=props["width"], height=props["height"] + 10)  # canvas size
            image = Image.open(os.path.join(os.getcwd(), "pictures", props["file"]))  # image path
            image.thumbnail((props["width"], props["height"]), Image.LANCZOS)  # Image size keep aspect ratio
            image_tk = ImageTk.PhotoImage(image)  # Necessary to work with an image in tkinter
            canvas.create_image(5, 4, image=image_tk, anchor=tk.NW)  # creates an image item on the Canvas
            draggable_widget = DraggableWidget(canvas)
            # The purpose of these setattr() calls is to create new attributes on the instance that reference
            # the Canvas, PhotoImage, and DraggableWidget objects that were created in the constructor
            setattr(self, f"{name}_canvas", canvas)
            setattr(self, f"{name}_image_tk", image_tk)
            setattr(self, f"draggable_{name}_image_canvas", draggable_widget)

        button_widgets = {
            "publish_button": {"text": "publish", "command": self.publish},
            "polar2geo_button": {"text": "polar2geo", "command": self.polar2geo},
            "geo2polar_button": {"text": "geo2polar", "command": self.geo2polar},
            # add more buttons here if needed
        }

        for name, props in button_widgets.items():
            button = tk.Button(self.root, text=props["text"], command=props["command"])
            draggable_widget = DraggableWidget(button)
            setattr(self, name, button)
            setattr(self, f"draggable_{name}", draggable_widget)

        # disable on the start because the default value is deg
        # we want to use these buttons just when the units of measure are radians
        self.polar2geo_button.configure(state='disabled')
        self.geo2polar_button.configure(state='disabled')

        self.save_button = tk.Button(self.root, text="Save", command=self.save_widgets)
        self.save_button.place(x=100, y=450)

        self.move_button = tk.Button(self.root, text="Move", command=self.move_widgets)
        self.move_button.place(x=150, y=450)

        # Makes sure that the same conversion value is not selected twice
        # (in the first case, makes sure that it is not a degree again)
        self.flag = True

        # connection to rti
        configDefsPath = 'my_generator/configDefs.json'
        with open(configDefsPath) as f:
            dictConfigDefs = json.load(f)
        sys.argv.append("my_generator/configDefs.json")
        sys.argv.append("dependencies/RtiConnectorModule.dll")
        self.rtiConnectorWrapperObject = RtiConnectorWrapper(dictConfigDefs)
        self.rtiConnectorWrapperObject.Create()

    def new_id(self):
        return uuid.uuid4()

    def uuid2int(self, ID):
        """
        translate uuid to int
        :param id:uuid object
        :return: 2 int_64 which represents input uuid (msb, lsb)
        """
        b_uuid = ID.int.to_bytes(16, byteorder=sys.byteorder, signed=False)
        # tmp_msb = int.from_bytes(b_uuid[8:], byteorder=sys.byteorder, signed=False)
        # tmp_lsb = int.from_bytes(b_uuid[:8], byteorder=sys.byteorder, signed=False)
        return struct.unpack('2q', b_uuid)

    def random_number(self):
        msb, lsb = self.uuid2int(self.new_id())
        self.msb_entry.delete(0, "end")
        self.msb_entry.insert(0, str(msb))
        self.lsb_entry.delete(0, "end")
        self.lsb_entry.insert(0, str(lsb))

    def polar2geo(self):
        if self.elevation_value_inside.get() != "deg":
            platform = (float(self.tank_latitude_entry.get()), float(self.tank_longitude_entry.get()),
                        float(self.tank_altitude_entry.get()))
            target_polar = (float(self.azimuth_entry.get()),
                            float(self.elevation_entry.get()), float(self.range_entry.get()))
            lat, lon, alt = new_polar2gps(platform, target_polar, transformer)
            polar_values = [lat, lon, alt]
            entrys = [self.target_latitude_entry, self.target_longitude_entry, self.target_altitude_entry]
            for entry, polar_value in zip(entrys, polar_values):
                entry.delete(0, "end")
                entry.insert(0, str(polar_value))

    def geo2polar(self):
        platform = (float(self.tank_latitude_entry.get()), float(self.tank_longitude_entry.get()),
                    float(self.tank_altitude_entry.get()))
        terget_geo = (float(self.target_latitude_entry.get()), float(self.target_longitude_entry.get()),
                      float(self.target_altitude_entry.get()))
        az, el, r = new_gps2polar(platform, terget_geo, transformer)
        geo_values = [az, el, r]
        entrys = [self.azimuth_entry, self.elevation_entry, self.range_entry]
        for entry, geo_value in zip(entrys, geo_values):
            entry.delete(0, "end")
            entry.insert(0, str(geo_value))

    def conversion_button_state(self):
        button_states = {
            "rad": "normal",
            "deg": "disabled",
        }
        buttons = [self.geo2polar_button, self.polar2geo_button]
        selected_values = [self.tank_latitude_value_inside, self.tank_longitude_value_inside,
                           self.elevation_value_inside, self.azimuth_value_inside,
                           self.target_latitude_value_inside, self.target_longitude_value_inside]
        # make sure that the conversion buttons will be available
        if self.tank_location_available_value_inside.get() == "True":
            if self.target_geo_available_value_inside.get() == "False":
                buttons[0].configure(state="disabled")
                if self.azimuth_value_inside.get() == "rad":  # check if you are in the right units
                    buttons[1].configure(state="normal")
                    return

            elif self.target_polar_available_value_inside.get() == "False":
                buttons[0].configure(state="normal")
                if self.target_latitude_value_inside.get() == "rad":
                    buttons[1].configure(state="disabled")
                    return
            # changes the state of the conversions button
            for conversion_button, selected_value in zip(buttons, selected_values):
                conversion_button.configure(state=button_states[selected_value.get()])
        else:
            buttons[0].configure(state="disabled")
            buttons[1].configure(state="disabled")

    # Checks that the same conversion type is not selected twice
    def deg_rad(self, option):
        if (option == "rad" and self.flag) or (option == "deg" and not self.flag):
            self.deg_rad_convert(option)
            self.flag = not self.flag
        # when you select deg the conversion_buttons need to be disabled and normal when you select rad
        self.conversion_button_state()

    # Converts from angle to radian and vice versa
    def deg_rad_convert(self, option):
        convert_func = {"rad": math.radians, "deg": math.degrees}[option]
        # Each entry you convert its value
        entries = [self.tank_latitude_entry, self.tank_longitude_entry, self.elevation_entry,
                   self.azimuth_entry, self.target_latitude_entry, self.target_longitude_entry]
        selected_values = [self.tank_latitude_value_inside, self.tank_longitude_value_inside,
                           self.elevation_value_inside, self.azimuth_value_inside, self.target_latitude_value_inside,
                           self.target_longitude_value_inside]
        disabled_entrys = []
        for entry, selected_value in zip(entries, selected_values):  # zip - allow me to  read from 2 lists
            # changing the disabled entrys state to normal in order to make the conversion
            if entry['state'] == tk.DISABLED:
                disabled_entrys.append(entry)
                entry.configure(state="normal")

            selected_value.set(option)
            value = float(entry.get())
            converted_value = convert_func(value)
            entry.delete(0, "end")  # delete the value inside the entry, so it could set another one instead
            entry.insert(0, str(converted_value))

            # Checks whether there are disabled entrys that changed to normal for the conversion
            if len(disabled_entrys) > 0:
                disabled_entrys.pop(0).configure(state="disable")

    def publish(self):

        my_generator.my_detection_generator.optronic_publisher(self)
        self.random_number()

    def type_selected(self, value):
        if value == "DETECTION":
            self.value = 0
        elif value == "THREAT":
            self.value = 1
        elif value == "TARGET":
            self.value = 2
        print(self.value)

    def on_version_selected(self, value):
        classification_map = {
            "1.14.1": {"Person": ["Person", "Car", "Motorcycle", "Marker"]},
            "1.22.1": {"Windcoat": ["Windcoat"]},
            "1.21.1": {"General Enemy": ["General Enemy", "Infantry", "AT", "AFV", "SA", "IFV"]},
            "1.2.1": {"General Enemy": ["General Enemy", "Infantry", "AT", "AFV", "SA", "IFV"]},
            "1.10.1": {"General Enemy": ["General Enemy", "Infantry", "AT", "AFV", "SA", "IFV"]},
            "1.11.1": {"General Enemy": ["General Enemy", "Infantry", "AT", "AFV", "SA", "IFV"]}
        }

        if value in classification_map:
            classifications = classification_map[value]  # take the dict base on the id
            for key, values in classifications.items():  # key - default value, values - values you want for this id
                self.classification_value_inside.set(key)
                self.classification_menu['menu'].delete(0, 'end')  # deletes the existing menu items
                # populates it with new menu items
                for value in values:
                    self.classification_menu['menu'].add_command(
                        label=value, command=lambda value=value: self.classification_value_inside.set(value))
                break

    # NOTE TO MYSELF to change it to that it will take the ones with the right name
    # instead of not taking the ones with the wrong name

    # saving the locations of each widget in a file
    def save_widgets(self):
        self.disable_dragging()
        with open("widget_positions.txt", "w") as f:
            count = 0
            not_in_the_name = ['draggable', 'save', 'move', 'root', 'value', 'flag', 'image', 'rti']
            for attribute_name, value in vars(self).items():
                #  checks if any of the items in the not_in_the_name list are present in the attribute_name string.
                #  If none of them are present, the not any() function returns True
                if not any(item in attribute_name for item in not_in_the_name):
                    x = getattr(self, attribute_name).winfo_x()
                    y = getattr(self, attribute_name).winfo_y()
                    f.write(f"{attribute_name}: {x},{y}\n")
                    count += 1

    def move_widgets(self):
        self.enable_dragging()

    # for every attribute in the class that start with "draggable", change her dragging attribute value to True
    def enable_dragging(self):
        for attr_name in dir(self):
            if attr_name.startswith("draggable"):
                attr = getattr(self, attr_name)
                attr.dragging = True

    # for every attribute in the class that start with "draggable", change her dragging attribute value to False
    def disable_dragging(self):
        for attr_name in dir(self):
            if attr_name.startswith("draggable"):
                attr = getattr(self, attr_name)
                attr.dragging = False

    def load_widget_positions(self):
        if os.path.isfile("widget_positions.txt"):
            with open("widget_positions.txt", "r") as f:
                for line in f:
                    try:
                        # Separates each line for name and position
                        key, value = line.strip().split(" ")
                        x, y = map(int, value.split(","))
                        # get the widget by key and set its position
                        widget_name = key.split(":")[0]
                        # retrieves the widget object based on the widget name in the key
                        widget = getattr(self, widget_name)
                        widget.place(x=x, y=y)

                    except Exception as e:
                        print(f"Error parsing widget position: {e}")

    def menu_clicked(self, options):
        # Define a dictionary that maps checkbutton values to entry states
        widgets_states = {
            "True": "normal",
            "False": "disabled",
        }

        # Define lists of widgets to update
        # tank_widgets = [self.tank_altitude_entry, self.tank_latitude_entry, self.tank_longitude_entry,
        #                 self.tank_latitude_menu, self.tank_longitude_menu]
        tank_widgets = [self.tank_altitude_entry, self.tank_latitude_entry, self.tank_longitude_entry]
        target_polar_widgets = [self.elevation_entry, self.azimuth_entry, self.range_entry, self.elevation_menu,
                                self.azimuth_menu, self.target_geo_available_menu]
        target_geo_widgets = [self.target_altitude_entry, self.target_latitude_entry, self.target_longitude_entry,
                              self.target_latitude_menu, self.target_longitude_menu, self.target_polar_available_menu]

        # Get the selected values from the checkbuttons
        tank_location_available = self.tank_location_available_value_inside.get()
        target_polar_available = self.target_polar_available_value_inside.get()
        target_geo_available = self.target_geo_available_value_inside.get()

        # Update the state of the widgets based on the selected values
        for widget in tank_widgets:
            widget.configure(state=widgets_states[tank_location_available])

        for widget in target_polar_widgets:
            widget.configure(state=widgets_states[target_polar_available])

        for widget in target_geo_widgets:
            widget.configure(state=widgets_states[target_geo_available])

        # every time you choose "True" or "False" I want it to also affect the conversion_button_state
        self.conversion_button_state()

    # stopping the script when you press "q" or "Q" in the keyboard
    def stop_mainloop(self, event):
        if event.char.lower() == "q":
            self.root.quit()

    def run(self):
        self.load_widget_positions()
        self.disable_dragging()
        self.on_version_selected("1.2.1")
        self.root.bind("<Key>", self.stop_mainloop)
        self.root.mainloop()


if __name__ == "__main__":
    app = App()
    app.run()
