import sys
import numpy as np
import uuid
from rtiwrapper.RtiConnectorWrapper import RtiConnectorWrapper
import json
import my_generator.utils
import time


def optronic_publisher(app, scen_name='my_generator/records/Day_N_650m_Positvie_Person.csv'):
    output_lst = my_generator.utils.load_scenario(scen_name)
    output_lst1 = my_generator.utils.data_parser(output_lst)
    # configDefsPath = 'my_generator/configDefs.json'
    # with open(configDefsPath) as f:
    #     dictConfigDefs = json.load(f)
    # sys.argv.append("my_generator/configDefs.json")
    # sys.argv.append("dependencies/RtiConnectorModule.dll")
    # rtiConnectorWrapperObject = RtiConnectorWrapper(dictConfigDefs)
    # rtiConnectorWrapperObject.Create()
    topicOptronics = "P_Tactical_Sensor_PSM::C_Detection_Optronics"
    for i, output in enumerate(output_lst1):
        # # print(f"sensorInertialLocationAvailable: {output['data'].A_spatialInfo.A_physicalInfo.A_sensorInertialLocationAvailable}")
        # print(f"tank_altitude: {output['data'].A_spatialInfo.A_physicalInfo.A_altitude_A_sensorInertialLocation}")
        # print(f"tank_latitude: {output['data'].A_spatialInfo.A_physicalInfo.A_latitude_A_sensorInertialLocation}")
        # print(f"tank_longitude: {output['data'].A_spatialInfo.A_physicalInfo.A_longitude_A_sensorInertialLocation}")
        #
        # # print(f"inertialLocationAvailable: {output['data'].A_spatialInfo.A_physicalInfo.A_inertialLocationAvailable}")
        # print(f"polar_elevation: {output['data'].A_spatialInfo.A_physicalInfo.A_elevation_A_inertialLocation}")
        # print(f"polar_azimuth: {output['data'].A_spatialInfo.A_physicalInfo.A_azimuth_A_inertialLocation}")
        # print(f"polar_range: {output['data'].A_spatialInfo.A_physicalInfo.A_range_A_inertialLocation}")
        #
        # # print(f"absoluteLocationAvailable: {output['data'].A_spatialInfo.A_physicalInfo.A_absoluteLocationAvailable}")
        # print(f"target_altitude: {output['data'].A_spatialInfo.A_physicalInfo.A_altitude_A_absoluteLocation}")
        # print(f"target_latitude: {output['data'].A_spatialInfo.A_physicalInfo.A_latitude_A_absoluteLocation}")
        # print(f"target_longitude: {output['data'].A_spatialInfo.A_physicalInfo.A_longitude_A_absoluteLocation}")

        empty_entry = True
        # making sure that there is no empty entry
        for attribute_name, value in vars(app).items():
            if "entry" in attribute_name and "draggable" not in attribute_name:
                entry = getattr(app, attribute_name)
                if entry.get() == '':
                    print(f"{attribute_name.replace('_entry', '')} must receive a value")
                    empty_entry = False

        if empty_entry is False:
            return

        source_id = str(app.source_id_value_inside.get()).split(".")
        output['data'].A_platformId_A_sourceID = int(source_id[0])
        output['data'].A_systemId_A_sourceID = int(source_id[1])
        output['data'].A_moduleId_A_sourceID = int(source_id[2])

        output['data'].A_detectionClassification = bytes(app.classification_value_inside.get(), 'utf-8')
        output['data'].A_detectionClassScore = float(app.classscore_entry.get())
        output['data'].A_type = app.value
        output['data'].A_spatialInfo.A_physicalInfo.A_sensorInertialLocationAvailable = \
            bool(app.tank_location_available_value_inside.get() == "True")
        output['data'].A_spatialInfo.A_physicalInfo.A_altitude_A_sensorInertialLocation = \
            float(app.tank_altitude_entry.get())
        output['data'].A_spatialInfo.A_physicalInfo.A_latitude_A_sensorInertialLocation = np.deg2rad(
            float(app.tank_latitude_entry.get()))
        output['data'].A_spatialInfo.A_physicalInfo.A_longitude_A_sensorInertialLocation = np.deg2rad(
            float(app.tank_longitude_entry.get()))
        output['data'].A_spatialInfo.A_physicalInfo.A_inertialLocationAvailable = bool(
            app.target_polar_available_value_inside.get() == "True")
        output['data'].A_spatialInfo.A_physicalInfo.A_elevation_A_inertialLocation = np.deg2rad(
            float(app.elevation_entry.get()))
        output['data'].A_spatialInfo.A_physicalInfo.A_azimuth_A_inertialLocation = np.deg2rad(
            float(app.azimuth_entry.get()))
        output['data'].A_spatialInfo.A_physicalInfo.A_range_A_inertialLocation = float(app.range_entry.get())
        output['data'].A_spatialInfo.A_physicalInfo.A_absoluteLocationAvailable = bool(
            app.target_geo_available_value_inside.get() == "True")
        output['data'].A_spatialInfo.A_physicalInfo.A_altitude_A_absoluteLocation = float(
            app.target_altitude_entry.get())
        output['data'].A_spatialInfo.A_physicalInfo.A_latitude_A_absoluteLocation = np.deg2rad(
            float(app.target_altitude_entry.get()))
        output['data'].A_spatialInfo.A_physicalInfo.A_longitude_A_absoluteLocation = np.deg2rad(
            float(app.target_longitude_entry.get()))
        output['data'].A_msb_A_detectionUniqueID = int(app.msb_entry.get())
        output['data'].A_lsb_A_detectionUniqueID = int(app.lsb_entry.get())
        output['data'].A_seconds_A_timeOfDataGeneration = int(time.time())
        output['data'].A_nanoseconds_A_timeOfDataGeneration = time.time_ns() % 1000000000

        app.rtiConnectorWrapperObject.Publish(topicOptronics, output['data'])

        print("published")


if __name__ == "__main__":
    print("hello")
