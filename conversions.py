import numpy as np
import pyproj
import pymap3d


def new_gps2polar(sensor, target, transfer):
    lat1, lon1, h1 = sensor
    lat2, lon2, h2 = target

    x1, y1 = transfer.transform(lat1, lon1, radians=True)  # x is easting, y is northing
    x2, y2 = transfer.transform(lat2, lon2, radians=True)
    dx = x2 - x1
    dy = y2 - y1
    dh = h2 - h1

    az = np.arctan2(dx, dy)
    r = np.sqrt(dx **2 + dy ** 2 + dh ** 2)
    el = np.arctan2(dh, np.hypot(dx, dy))
    return az, el, r


def new_polar2gps(sensor, target, transfer):  # target in polar, sensor in latlon
    az, el, r = target
    r_cos_el = r * np.cos(el)
    y2 = r_cos_el * np.cos(az)
    x2 = r_cos_el * np.sin(az)
    h2 = r * np.sin(el)

    x1, y1 = transfer.transform(sensor[0], sensor[1], radians=True)
    h1 = sensor[2]

    alt = h1 + h2
    x = x1 + x2
    y = y1 + y2
    lat, lon = transfer.transform(x, y, radians=True, direction="inverse")

    return lat, lon, alt


def gps2ecef_pyproj(lat, lon, alt):
    """
    from answer in
    https://stackoverflow.com/questions/18759601/converting-lla-to-xyz
    notice lon,lat here as GIS orders (oppisite to everywhere else)
    """
    ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
    lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    x, y, z = pyproj.transform(lla, ecef, lon, lat, alt, radians=True)  # TODO: radians false for checks, should be true rest of time
    # return x / 1000, y / 1000, z / 1000
    return x, y, z


def ecef2gps_pyproj(x, y, z):
    # x, y, z = x * 1000, y * 1000, z * 1000
    x, y, z = x, y, z
    ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
    lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    lon, lat, alt = pyproj.transform(ecef, lla, x, y, z, radians=True)  # TODO: radians false for checks, should be true rest of time
    return lat, lon, alt


def geoplat_and_sph_to_geotarget(sph_dis, platform):
    latp, lonp, altp = platform
    # az, el, range = sph_dis
    xyz_platform = gps2ecef_pyproj(latp, lonp, altp)
    xyz_movement = spherical_to_cartesian(sph_dis)
    return ecef2gps_pyproj(*(xyz_platform + xyz_movement))


def gps2sph_distance(platform, target):
    azimuth = get_bearing(platform[0], platform[1], target[0], target[1])
    distance = get_distance(platform[0], platform[1], target[0], target[1])
    height = target[2] - platform[2]
    elevation = np.arctan2(height, distance)
    return azimuth, elevation, distance  # this distance is the shadowed distance on ground and not actual "range"


def gps2sph_distance_pymap3d(platform, target):
    lat0, lon0, alt0 = platform
    lat1, lon1, alt1 = target
    az, el, r = pymap3d.aer.geodetic2aer(lat1, lon1, alt1, lat0, lon0, alt0, deg=False)
    if az < 0:
        az = (az + (2 * np.pi)) % (2 * np.pi)
        # print(f"pymap3d gave negative azimuth. fix this")
    return az, el, r


def get_distance(lat1, lon1, lat2, lon2):
    d_lat = lat2 - lat1
    d_lon = lon2 - lon1
    a = (np.sin(d_lat / 2) ** 2) + (np.cos(lat1) * np.cos(lat2) * (np.sin(d_lon / 2) ** 2))
    c = 2 * np.arctan2(a ** 0.5, (1 - a) ** 0.5)  # i find in internet that should be 2 * np.asin(a ** 0.5)
    return 6371000 * c  # maybe check 6378137 instead of 6371000


# def test(az, el, r, latp, lonp, altp):
#     h = r * np.tan(el)
#     altt = h + altp
#
#
#     c = r / 6371000
#     t = np.tan(c/2) ** 2
#     a = t / (1 + t)


def get_bearing(lat1, lon1, lat2, lon2):
    d_lon = lon2 - lon1
    bearing = np.arctan2(np.sin(d_lon) * np.cos(lat2), (np.cos(lat1) * np.sin(lat2)) - (np.sin(lat1) * np.cos(lat2) * np.cos(d_lon)))
    return (bearing + 2*np.pi) % (2 * np.pi)


# def gps2spherical_distance(platform, target):
#     xp, yp, zp = gps2ecef_pyproj(*platform)
#     xt, yt, zt = gps2ecef_pyproj(*target)
#     dx = xt - xp
#     dy = yt - yp
#     dz = zt - zp
#     reldist_cartesian = np.array([dx, dy, dz])
#     reldist_spherical = cartesian_to_spherical(reldist_cartesian)
#     return reldist_spherical


def meter2latlon(x, y):  # ori
    p = pyproj.Proj(proj='utm', zone=36, ellps='WGS84', preserve_units=False)
    # p = pyproj.Proj(proj='geocent', zone=36, ellps='WGS84', preserve_units=False)
    # lat, lon = p(x, y, inverse=True)
    lon, lat = p(x, y, inverse=True)
    return np.deg2rad(lat), np.deg2rad(lon)


def cartesian2geo_v2(reldist_cartesian, platform_geo):  # ori
    x1, y1, zone_number, zone_letter = utm.from_latlon(np.rad2deg(platform_geo[0]), np.rad2deg(platform_geo[1]))
    z1 = platform_geo[2]

    x2, y2, z2 = reldist_cartesian + np.array([x1, y1, z1])
    lat, lon = utm.to_latlon(x2, y2, zone_number, zone_letter)
    alt = z2
    return np.deg2rad(lat), np.deg2rad(lon), alt


def cartesian2geo(reldist_cartesian, platform_geo):  # ori
    """gets target relative xyz from platform, and platform lla - returns target lla"""
    x1, y1 = latlon2meter(*platform_geo[0:2])
    z1 = platform_geo[2]

    x2, y2, z2 = reldist_cartesian + np.array([x1, y1, z1])
    lat, lon = meter2latlon(x2, y2)
    alt = z2
    # lat, lon, alt = ecef2gps_pyproj(x2, y2, z2)
    return lat, lon, alt


def spherical2geo(reldist_spherical, platform_geo):  # ori
    """gets the relative spherical target az,el,r from platform, and the lla of platform - returns target lla"""
    reldist_cartesian = spherical_to_cartesian(reldist_spherical)
    a = np.array(cartesian2geo(reldist_cartesian, platform_geo))
    # b = np.array(cartesian2geo_v2(reldist_cartesian, platform_geo))
    # print(a - b)
    return a


def spherical2geo_pymap3d(reldist_spherical, platform_geo):
    lat0, lon0, alt0 = platform_geo
    # e, n, u = spherical_to_cartesian(reldist_spherical)
    # return np.array(pymap3d.enu.enu2geodetic(e, n, u, lat0, lon0, alt0, deg=False))
    a, e, r = reldist_spherical
    return np.array(pymap3d.aer.aer2geodetic(a, e, r, lat0, lon0, alt0, deg=False))


def latlon2meter(lat, lon):
    p = pyproj.Proj(proj='utm', zone=36, ellps='WGS84', preserve_units=False) # TODO: Inaccurate and relies on UTM for conversion. Change this to something more robust. (from old)
    # p = pyproj.Proj(proj='geocent', zone=36, ellps='WGS84', preserve_units=False)
    # x, y = p(np.rad2deg(lat), np.rad2deg(lon))
    x, y = p(np.rad2deg(lon), np.rad2deg(lat))
    return x, y


def coord2reldist(platform, target):  # TODO: hate these names its from neyman or someone. maybe change to spherical_distance?
    x1, y1 = latlon2meter(*platform[0:2])
    x2, y2 = latlon2meter(*target[0:2])
    z1 = platform[2]
    z2 = target[2]
    reldist_cartesian = np.array([x2 - x1, y2 - y1, z2 - z1])
    reldist_spherical = cartesian_to_spherical(reldist_cartesian)
    return reldist_spherical


def cartesian_to_spherical(points_cartesian: np.ndarray) -> np.ndarray:  # TODO: this is sph distance
    """
    Converts cartesian distance coordinate array to spherical.
    [N x 3] -> [N x 3]
    :param points_cartesian:
    :return:
    """
    if len(np.shape(points_cartesian)) == 1:
        x, y, z = points_cartesian
    else:
        x = points_cartesian[:, 0]
        y = points_cartesian[:, 1]
        z = points_cartesian[:, 2]

    xysq = np.square(x) + np.square(y)
    xy = np.sqrt(xysq)
    r = np.sqrt(xysq + np.square(z))
    azimuth = np.arctan2(x, y)  # np.arctan2(y, x)  # TODO: should be x,y no? check it
    elevation = np.arctan2(z, xy)

    points_spherical = np.column_stack((azimuth, elevation, r)).squeeze()  # meaning of r? from earth center? it comes out way too big

    return points_spherical


def spherical_to_cartesian(points_spherical: np.ndarray) -> np.ndarray:
    """
    Converts spherical coordinate array to cartesian.
    [N x 3] -> [N x 3]
    :param points_spherical:
    :return:
    """
    if len(np.shape(points_spherical)) == 1:
        azimuth, elevation, r = points_spherical[0], points_spherical[1], points_spherical[2]
    else:
        azimuth = points_spherical[:, 0]
        elevation = points_spherical[:, 1]
        r = points_spherical[:, 2]
    r_cos_el = r * np.cos(elevation)
    # x = r_cos_el * np.cos(azimuth)
    # y = r_cos_el * np.sin(azimuth)
    y = r_cos_el * np.cos(azimuth)
    x = r_cos_el * np.sin(azimuth)
    z = r * np.sin(elevation)

    points_cartesian = np.column_stack((x, y, z)).squeeze()

    return points_cartesian


if __name__ == "__main__":
    # az, el, r = 0.5, 0.8, 50  # radians
    # lat, lon = np.deg2rad((32.100460, 35.206991))
    # alt = 680
    # platform_geo = (lat, lon, alt)
    #
    # target_geo = spherical2geo((az, el, r), platform_geo)
    # print(f"target geo {target_geo} rads")
    # print(f"target geo {np.rad2deg(target_geo[:2])} degs")
    #
    # reconstructed_target_sph = coord2reldist(platform_geo, target_geo)
    # print(reconstructed_target_sph)
    #
    # print("#######")
    # ecef_new = gps2ecef_pyproj(*target_geo)
    # print(ecef_new)
    # spher_from_ecef = cartesian_to_spherical(ecef_new)
    # print(np.rad2deg(spher_from_ecef[:2]), spher_from_ecef[-1])
    # print(ecef2gps_pyproj(*ecef_new))

    p1 = 32.01812241899772, 34.821286966788804, 200
    p2 = 32.043558204090154, 34.841040514437154, 240
    p1r = np.deg2rad(32.01812241899772), np.deg2rad(34.821286966788804), 200
    p2r = np.deg2rad(32.043558204090154), np.deg2rad(34.841040514437154), 240

    sph_dis_1 = gps2sph_distance(p1r, p2r)
    tar_recon_1 = spherical2geo(sph_dis_1, p1r)
    # tar_recon_1_2 = geoplat_and_sph_to_geotarget(sph_dis_1, p1r)
    print(sph_dis_1)
    print(np.rad2deg(tar_recon_1[:2]), tar_recon_1[2])
    # print(np.rad2deg(tar_recon_1_2[:2]), tar_recon_1_2[2])


    import time
    sensor_deg = np.array([31.137380, 34.549687, 240])
    lat, lon, alt = np.array([np.deg2rad(sensor_deg[0]), np.deg2rad(sensor_deg[1]), sensor_deg[2]])
    arr = []
    arr2 = []
    for i in range(100):
        start = time.time()
        x, y, z = gps2ecef_pyproj(lat, lon, alt)
        rlat, rlon, ralt = ecef2gps_pyproj(x, y, z)
        end = time.time()
        total = end - start
        arr.append(total)

        start = time.time()
        x, y, z = pymap3d.ecef.geodetic2ecef(lat, lon, alt, deg=False)
        rlat, rlon, ralt = pymap3d.ecef.ecef2geodetic(x, y, z, deg=False)
        end = time.time()
        total = end - start
        arr2.append(total)

    arr = np.array(arr)
    arr2 = np.array(arr2)

    print(f"min: {arr.min()} vs {arr2.min()}")
    print(f"max: {arr.max()} vs {arr2.max()}")
    print(f"mean: {arr.mean()} vs {arr2.mean()}")
    print(f"median: {np.median(arr)} vs {np.median(arr2)}")
